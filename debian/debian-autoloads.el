;;;###autoload
(setq elpy-rpc-pythonpath nil
      python-shell-interpreter "/usr/bin/python3"
      python-shell-interpreter-args "-i -E"
      elpy-rpc-python-command "/usr/bin/python3"
      elpy-rpc-virtualenv-path 'system)
